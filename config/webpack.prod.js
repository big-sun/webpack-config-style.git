const webpackBaseConfig = require('./webpack.base.js');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
let MiniCssExtractPlugin = require('mini-css-extract-plugin');
let OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const { merge } = require('webpack-merge');

const webpackProdConfig = {
  mode: 'production',
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[name].chunk.css'
    })
  ],
  optimization: {
    minimizer: [new OptimizeCssAssetsWebpackPlugin({})]
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: { //保证@import引入其他样式文件的时候,重新走前面的postcss-loader
              importLoaders: 2
            }
          },
          {//为css样式添加厂商前缀
            loader: "postcss-loader"
          },
          {//将scss文件转换成css文件
            loader: 'sass-loader'
          }
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: { //保证@import引入其他样式文件的时候,重新走前面的postcss-loader
              importLoaders: 1
            }
          },
          {//为css样式添加厂商前缀
            loader: "postcss-loader"
          }
        ],
      },
    ]
  }
};

module.exports = merge(webpackBaseConfig, webpackProdConfig);