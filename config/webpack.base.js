const path = require('path');

module.exports = {
  entry: {
    index: './src/main.js'
  },
  module: {
    rules: [
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10 * 1024,//限制大小
            outputPath: 'fonts/',//复制出来的字体文件输出路径
            name: '[name].[hash:7].[ext]'//复制出来的字体图标的名称
          }
        }]
      }
    ]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, '../dist')
  }
};