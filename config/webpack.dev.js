const webpackBaseConfig = require('./webpack.base.js');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { merge } = require('webpack-merge');

const NODE_ENV = process.env.NODE_ENV;//获取package.json中配置的环境
let plugins = [
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: './index.html',
    inject: true
  }),
];//定义一个plugins的空数组
if (NODE_ENV == 'production') {//如果是打包环境
  plugins.push(new CleanWebpackPlugin())
}

const webpackDevConfig = {
  mode: 'development',
  plugins: plugins,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: { //保证@import引入其他样式文件的时候,重新走前面的postcss-loader
              importLoaders: 2
            }
          },
          {//为css样式添加厂商前缀
            loader: "postcss-loader"
          },
          {//将scss文件转换成css文件
            loader: 'sass-loader'
          }
        ],
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: { //保证@import引入其他样式文件的时候,重新走前面的postcss-loader
              importLoaders: 1
            }
          },
          {//为css样式添加厂商前缀
            loader: "postcss-loader"
          }
        ],
      },
    ]
  }
};

module.exports = merge(webpackBaseConfig, webpackDevConfig);